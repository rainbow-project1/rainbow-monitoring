import time
import yaml
import os

from RainbowMonitoringSDK.controller import Controller

if __name__ == '__main__':
    print("Monitoring Initialization")
    document = open(os.environ['CONFIG_FILE'] if 'CONFIG_FILE' in os.environ else '/code/configs.yaml', "r")
    configs = yaml.safe_load(document)
    if 'STORAGE_AGENT_HOSTNAME' in os.environ and 'STORAGE_AGENT_PORT' in os.environ:
        if not'dissemination-units' in configs:
            configs['dissemination-units'] = None
        if configs['dissemination-units'] is None or not'IgniteExporter' in configs['dissemination-units']:
            configs['dissemination-units'] = {'IgniteExporter': {'hostname': 'ignite-server', 'port': 50000}}
        configs['dissemination-units']['IgniteExporter']['hostname'] = os.environ['STORAGE_AGENT_HOSTNAME']
        configs['dissemination-units']['IgniteExporter']['port'] = os.environ['STORAGE_AGENT_PORT']
    if 'APPLICATION_URLS' in os.environ: #172.20.226.20:8080/metric
        if not 'sensing-units' in configs:
            configs['sensing-units'] = {}
        configs['sensing-units']['WebUserDefinedMetrics'] = {
            'periodicity': '5s',
            'sources': os.environ.get('APPLICATION_URLS').split(',')
            }
    controller = Controller(configs)
    controller.instantiate_dissemination_units()
    controller.instantiate_sensing_units()
    controller.start_sensing_units()
    controller.start_dissemination_units()
    controller.start_control()
