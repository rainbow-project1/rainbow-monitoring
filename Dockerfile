FROM netdata/netdata
ENV PYTHONUNBUFFERED=1

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools


RUN mkdir /code
ADD ./RainbowMonitoringSDK/code /code/
ADD ./RainbowMonitoringSDK/requirements.txt /code/requirements.txt
WORKDIR /code
RUN pip3 install -r requirements.txt
ADD configs.yaml /code/configs.yaml



ENTRYPOINT ["python3", "/code/main.py"]
