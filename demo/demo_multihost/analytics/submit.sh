MAIN_CLASS="eu.rainbowh2020.Main"
TOPOLOGY="streamsight-topology"
NETWORK="rainbow-net"
STREAMSIGHT_FILE="scenario1.insights"
#TODO
STORAGE_SERVERS="HOSTNAME1,HOSTNAME2,HOSTNAME3"

#TODO: DOES NOT BUILD THE ARTIFACT
#mvn clean antlr4:antlr4 package -DskipTests

docker run -it --rm --network $NETWORK storm storm kill $TOPOLOGY 2>&1  > /dev/null
if [ "$?" -ne "1" ]
then
  echo "Existing topology deleted, waiting 15s..."
  sleep 15
fi
docker run -it --rm --network $NETWORK  \
  -e STORAGE_SERVERS=$STORAGE_SERVERS \
  -v $(pwd)/analytics-1.0-SNAPSHOT.jar:/analytics-1.0-SNAPSHOT.jar \
  -v $(pwd)/$STREAMSIGHT_FILE:/insights.insights \
    storm storm jar /analytics-1.0-SNAPSHOT.jar $MAIN_CLASS $TOPOLOGY


#-v $(pwd)/target/analytics-1.0-SNAPSHOT.jar:/analytics-1.0-SNAPSHOT.jar \
