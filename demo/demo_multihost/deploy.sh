#!/bin/bash
if [ "$1" = "start" ]; then
	echo "Creating docker networks"
	docker network create -d overlay --attachable rainbow-net
	echo "Deploying the zoo stack"
	#We need to first create the zookeeper and an instance of ignite for the creation of the cluster
	docker stack deploy -c docker-stack-zoo.yaml zoo
	#Wait 5 seconds for sanity check
	sleep 5
	echo "Deploying the rest stuff stack"
	#Deploy the rest of the services (storage, monitoring, analytics, application)
	docker stack deploy -c docker-stack-rest.yaml demo
elif [ "$1" = "stop" ]; then
	echo "Stoping the stack"
	docker stack rm zoo
	docker stack rm demo
else
	echo "Usage: $(basename "$0") (start|stop)"
fi
exit 0

