# Multihost demo

The compose files are setup to work with docker swarm and 4 machines. 1 manager and 3 workers.

For the `monitoring_conf/mon_config.yaml` file change for each worker/fog node:

* node_id

* local hostname/ip for storage output

Each config file should be in the path that the docker-compose monitoring service declares, for each respective machine/node.

For the `analytics/scenario1.insights` file change the `entityID` for each stream.

For the `docker-compose` files change the storage environment variable for the Storm services to the hostnames/ips of the worker machines.

Run `./deploy.sh start` to start the cluster in 2 stacks.

Every change that is mandatory/optional in the files is annotated with a `TODO` keyword.
