from random import randint

from flask import Flask, request
from probes.utils import RainbowUtils
app = Flask(__name__)

@RainbowUtils.export("RANDOM_INT", "none", "A random number")
def rand_number():
    return randint(1, 100)

@app.route("/", methods=['POST'])
@RainbowUtils.timeit
@RainbowUtils.countit
def save():
    rand_number()
    myFile = open("data", 'a+')
    myFile.write(str(request.data))
    myFile.close()
    return "{'success':'true'}"



if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=8000)